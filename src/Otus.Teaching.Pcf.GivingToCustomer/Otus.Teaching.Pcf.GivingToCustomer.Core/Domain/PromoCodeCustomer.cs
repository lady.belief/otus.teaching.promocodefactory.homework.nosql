﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCodeCustomer : BaseEntity
    {
        public Guid PromoCodeId { get; set; }
        public virtual PromoCodeDto PromoCode { get; set; }

        public Guid CustomerId { get; set; }
        public virtual CustomerDto Customer { get; set; }
    }
    
    public class PromoCodeDto : BaseEntity
    {
        public PromoCodeDto(PromoCode promoCode)
        {
            Id = promoCode.Id;
            Code = promoCode.Code;
            BeginDate = promoCode.BeginDate;
            EndDate = promoCode.EndDate;
            PartnerId = promoCode.PartnerId;
            ServiceInfo = promoCode.ServiceInfo;
            PreferenceId = promoCode.PreferenceId;
        }
        
        public string Code { get; set; }
        public string ServiceInfo { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid PartnerId { get; set; }
        public Guid PreferenceId { get; set; }
    }
    
    public class CustomerDto : BaseEntity
    {
        public CustomerDto(Customer customer)
        {
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
        }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string Email { get; set; }
    }
}
