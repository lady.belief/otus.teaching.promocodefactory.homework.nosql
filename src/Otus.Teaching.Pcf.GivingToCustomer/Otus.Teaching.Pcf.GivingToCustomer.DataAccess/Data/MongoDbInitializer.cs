﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        public MongoDbInitializer(IConfiguration settings)
        {
            _settings = settings.GetSection("PromocodeFactoryGivingToCustomerDb").Get<GivingToCustomerDatabaseSettings>();
            var mongoClient = new MongoClient(_settings.ConnectionString);
            _mongoDb = mongoClient.GetDatabase(_settings.DatabaseName);
        }
        
        public void InitializeDb()
        {
            var collectionC = _mongoDb.GetCollection<Customer>(
                GivingToCustomerDatabaseSettings.GetCollectionName(_settings,typeof(Customer)));
            collectionC.InsertManyAsync(FakeDataFactory.Customers);
            
            var collectionP = _mongoDb.GetCollection<Preference>(
                GivingToCustomerDatabaseSettings.GetCollectionName(_settings,typeof(Preference)));
            collectionP.InsertManyAsync(FakeDataFactory.Preferences);
        }

        private readonly IMongoDatabase _mongoDb;
        private readonly IGivingToCustomerDatabaseSettings _settings;
    }
}