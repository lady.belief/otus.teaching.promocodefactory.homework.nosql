﻿using System;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public interface IGivingToCustomerDatabaseSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
        string CustomerCollectionName { get; set; }
        string CustomerPreferenceCollectionName { get; set; }
        string PreferenceCollectionName { get; set; }
        string PromoCodeCollectionName { get; set; }
        string PromoCodeCustomerCollectionName { get; set; }
    }
    
    public class GivingToCustomerDatabaseSettings : IGivingToCustomerDatabaseSettings
    {
        public static string GetCollectionName(IGivingToCustomerDatabaseSettings settings, Type t)
        {
            return t.Name switch
            {
                nameof(Customer) => settings.CustomerCollectionName,
                nameof(CustomerPreference) => settings.CustomerPreferenceCollectionName,
                nameof(Preference) => settings.PreferenceCollectionName,
                nameof(PromoCode) => settings.PromoCodeCollectionName,
                nameof(PromoCodeCustomer) => settings.PromoCodeCustomerCollectionName,
                _ => string.Empty
            };
        }
        
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string CustomerCollectionName { get; set; }
        public string CustomerPreferenceCollectionName { get; set; }
        public string PreferenceCollectionName { get; set; }
        public string PromoCodeCollectionName { get; set; }
        public string PromoCodeCustomerCollectionName { get; set; }
    }
}